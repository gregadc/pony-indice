"""Search index for Django."""

VERSION = (1, 0, 1)
__version__ = '.'.join(str(i) for i in VERSION)
__license__ = 'BSD License'
__url__ = 'https://gitlab.com/ZuluPro/pony-indice'
__author__ = 'Anthony Monthe (ZuluPro)'
__email__ = 'anthony.monthe@gmail.com'

default_app_config = 'pony_indice.apps.PonyIndiceConfig'
